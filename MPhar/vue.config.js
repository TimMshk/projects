const StyleLintPlugin = require('stylelint-webpack-plugin');
const CopyPlugin = require( 'copy-webpack-plugin' );
const { resolve: resolvePath } = require( 'path' );
const path = require('path');
const srcPath = resolvePath( __dirname, 'src' );
const distPath = resolvePath( __dirname, 'dist' );
const publicPath = resolvePath( __dirname, 'public' );

module.exports = {
  configureWebpack: {
    plugins: [
      new StyleLintPlugin({
        configFile: 'stylelint.js',
        files: ['**/*.{vue,htm,html,css,less,scss,sass}'],
      }),
      new CopyPlugin({
        patterns:[
          {
            from: '{config.js,contents.css,styles.js,adapters/**/*,lang/**/*,plugins/**/*,skins/**/*,vendor/**/*}',
            to: resolvePath( distPath, 'ckeditor' ),
            context: resolvePath( srcPath, 'ckeditor' )
          },
        ]
      }),
    ],
  },
  css: {
    loaderOptions: {
      scss: {
        prependData: '@import "~@/assets/scss/_variables.scss"; @import "~@/assets/scss/_functions.scss"; @import "~@/assets/scss/_mixins.scss";',
      },
    },
  },

};
