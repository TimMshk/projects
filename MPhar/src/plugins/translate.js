import store from '@/store';
import Vue from 'vue';

Vue.prototype.$t = (fieldName, defaultText) => {
  const lang = store.getters['platform/lang'];
  return lang?.[fieldName] || defaultText;
};
