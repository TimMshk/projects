const languages = [
  {
    label: 'Albanian',
    value: 'sq',
  },
  {
    label: 'Arabic',
    value: 'ar',
  },
  {
    label: 'Belarusian',
    value: 'be',
  },
  {
    label: 'Bosnian',
    value: 'bs',
  },
  {
    label: 'Bulgarian',
    value: 'bg',
  },
  {
    label: 'Chinese',
    value: 'zh',
  },
  {
    label: 'Croatian',
    value: 'hr',
  },
  {
    label: 'Czech',
    value: 'cs',
  },
  {
    label: 'Danish',
    value: 'da',
  },
  {
    label: 'Dutch',
    value: 'nl',
  },
  {
    label: 'English',
    value: 'en',
  },
  {
    label: 'Estonian',
    value: 'et',
  },
  {
    label: 'Filipino',
    value: 'tl',
  },
  {
    label: 'Finnish',
    value: 'fi',
  },
  {
    label: 'French',
    value: 'fr',
  },
  {
    label: 'Georgian',
    value: 'ka',
  },
  {
    label: 'German',
    value: 'de',
  },
  {
    label: 'Greek',
    value: 'el',
  },
  {
    label: 'Hungarian',
    value: 'hu',
  },
  {
    label: 'Icelandic',
    value: 'is',
  },
  {
    label: 'Indonesian',
    value: 'id',
  },
  {
    label: 'Italian',
    value: 'it',
  },
  {
    label: 'Japanese',
    value: 'ja',
  },
  {
    label: 'Korean',
    value: 'ko',
  },
  {
    label: 'Latvian',
    value: 'lv',
  },
  {
    label: 'Lithuanian',
    value: 'lt',
  },
  {
    label: 'Macedonian',
    value: 'mk',
  },
  {
    label: 'Mongolian',
    value: 'mn',
  },
  {
    label: 'Norwegian',
    value: 'no',
  },
  {
    label: 'Polish',
    value: 'pl',
  },
  {
    label: 'Portuguese',
    value: 'pt',
  },
  {
    label: 'Romanian',
    value: 'ro',
  },
  {
    label: 'Russian',
    value: 'ru',
  },
  {
    label: 'Serbian',
    value: 'sr',
  },
  {
    label: 'Slovak',
    value: 'sk',
  },
  {
    label: 'Slovenian',
    value: 'sl',
  },
  {
    label: 'Spanish',
    value: 'es',
  },
  {
    label: 'Swedish',
    value: 'sv',
  },
  {
    label: 'Thai',
    value: 'th',
  },
  {
    label: 'Turkish',
    value: 'tr',
  },
  {
    label: 'Ukrainian',
    value: 'uk',
  },
  {
    label: 'Vietnamese',
    value: 'vi',
  },
];

export default languages;
