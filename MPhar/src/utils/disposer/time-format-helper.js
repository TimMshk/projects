import moment from 'moment';

export const uniteTime = (date, time) => {
    moment.locale('ru');
    return `${moment(date).locale('ru').format('YYYY-MM-DD')} ${moment(time).format('LTS')}`;
};
