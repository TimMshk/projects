export const DOCUMENT_TEMPLATE = (id) => ({
      id: id,
      secured: false,
      watermark: false,
      name: '',
      file: null,
      openLink: '',
});

export const VIDEO_TEMPLATE = (id) => ({
  id: id,
  secured: false,
  name: '',
  file: null,
  cover: null,
  coverUrl: '',
  openLink: '',
  preview: null,
  description: '',
});

export const LINK_TEMPLATE = (id) => ({
  id: id,
  link: '',
  description: '',
});
