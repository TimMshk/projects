export const  parseImg = (img) => new Promise((resolve) => {
  const reader = new FileReader();
  reader.readAsDataURL(img);
  reader.onload = (e) => {
    resolve(e.target.result);
  };
});
