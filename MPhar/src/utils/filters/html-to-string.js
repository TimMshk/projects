export default function(html) {
    if (!html) return html;
    html = html.replace('<br />', ' ');
    html = html.replace('\n', ' ');
    html = html.replace('\r', ' ');
    const tempDivElement = document.createElement('div');
    tempDivElement.innerHTML = html;
    return tempDivElement.textContent || tempDivElement.innerText || '';
}
