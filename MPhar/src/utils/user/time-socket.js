import store from '@/store';
import router  from '@/router';

import { socketInstance, GP } from '@/utils/sockets-helper';

const NAME_SPACE = 'time';
const TIME_INFO = 'info';
const ACTIVITY = 'activity';
const INACTIVE = 'inactive';
const DISCONNECT = 'disconnect';
const LOGOUT = 'logout';
const CHANGE_TOPIC = 'change-topic';

const generateSocketPath = new GP(NAME_SPACE);

let timeSocket = null;

export const initTimeSocket = () => {
  timeSocket = socketInstance(NAME_SPACE);

  timeSocket.on(generateSocketPath.generate(TIME_INFO), data => {
    store.dispatch('auth/changeTime', data.current_time);
  });

  timeSocket.on(generateSocketPath.generate(INACTIVE), () => {
    store.dispatch('auth/toggleTimerWarning', true);
  });

  timeSocket.on(generateSocketPath.generate(DISCONNECT), async () => {
    logoutTopicTimer();
    await store.dispatch('auth/logout');
    router.push({ name: 'login' });
    store.dispatch('auth/toggleTimerWarning', false);
  });

  let canSendActivity = true;


  window.addEventListener('mousemove', touchHandler);
  window.addEventListener('scroll', touchHandler);
  window.addEventListener('touchmove', touchHandler);

  function touchHandler() {
      if (!timeSocket) return;
      if (canSendActivity) {
          canSendActivity = false;
          setTimeout(()=>{
              canSendActivity = true;
          }, 5000);
          timeSocket.emit(generateSocketPath.generate(ACTIVITY));
          store.dispatch('auth/toggleTimerWarning', false);
      }
  }

  window.addEventListener('blur', function() {
    changeTopicTimer(0);
  });

  window.addEventListener('focus', function() {
    if (router.currentRoute.name === 'dashboard') {
      changeTopicTimer(store.getters['dashboard/topicId']);
    }
  });
};

export const changeTopicTimer = (id) => {
  if (timeSocket) {
    timeSocket.emit(generateSocketPath.generate(CHANGE_TOPIC), { topic_id : id });
  }
};

export const logoutTopicTimer = () => {
  if (timeSocket) {
    timeSocket.emit(generateSocketPath.generate(LOGOUT));
    timeSocket.disconnect();
    timeSocket = null;
  }
};
