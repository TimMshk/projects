/**
 * @license Copyright (c) 2003-2020, CKSource - Frederico Knabben. All rights reserved.
* For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
    */


let BASE_URL =  'https://api.newabplatform.we-demonstrate2.ru';

if (window.location.origin==='https://abplatforms.org') {
    BASE_URL = 'https://api.abplatforms.org';
}

const UPLOAD_IMG_URL = `${BASE_URL}/files/upload-image`;

CKEDITOR.editorConfig = function( config ) {
    config.allowedContent = true;
    config.removePlugins = 'easyimage,elementspath,pastetext,pastetools,pastefromgdocs,pastefromword';

    config.imageUploadURL = UPLOAD_IMG_URL;
    config.filebrowserImageUploadUrl = UPLOAD_IMG_URL;
    config.cloudServices_tokenUrl = UPLOAD_IMG_URL;
    config.forcePasteAsPlainText = true;


    config.dataParser = function(data){
        return data.data.url;
    };
    config.enterMode = CKEDITOR.ENTER_BR;

};

