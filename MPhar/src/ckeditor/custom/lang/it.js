const langs = {
    'it': {
        'video' : 'Partecipa alla videochiamata',
        'new-chapter': 'Nuovo capitolo',
        'save': 'Mantenere',
        'voting': 'Nuovo voto',
        'vote-deleted': 'Questo voto è stato rimosso dal suo autore',
        'vote-remove': 'Vuoi rimuovere questa votazione?',
        'vote-restore': 'Vuoi ripristinare questa votazione?',
        'saving-change': 'Risparmio...',
        'saved-change': 'Salvare',
        'not-saved-change': 'Non salvato',
        'warning-empty-doc': 'Il documento è vuoto',
        'warning-empty-title': 'Il titolo è vuoto',
        'warning-empty-voting': 'Domanda vuota o opzioni',
        'count-votes': 'voti',
        'chapter-head': 'Nuovo capitolo',
        'chapter-title-label': 'Titolo',
        'voting-head': 'Nuova votazione',
        'voting-question-label': 'Domanda',
        'voting-options-label': 'Opzioni',
        'voting-table-option': 'Opzione',
        'voting-table-action': 'Azione',
        'form-cancel': 'Annullare',
        'image-loading': 'Si prega di attendere fino a quando l\'immagine viene caricata...',
        'warning-big-image': 'File troppo grande! Si prega di scaricare le dimensioni dell\'immagine fino a 5 MB',
        'warning-format-image': 'Il file scaricato non è un\'immagine! Si prega di caricare l\'immagine',
        'warning-error-image': 'L\'immagine non è stata caricata! Si prega di riprovare!'
    }
};

