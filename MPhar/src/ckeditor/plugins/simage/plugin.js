
CKEDITOR.plugins.add( 'simage', {
	icons: 'simage',
	allowedContent: 'img[alt,!src,width,height,data-width,data-height]{border-style,border-width,float,height,margin‌​,margin-bottom,margi‌​n-left,margin-right,‌​margin-top,width}',
	init: function( editor ) {
		editor.addCommand('simage', {
			exec: function (editor) {
				a = document.createElement('input')
				a.setAttribute('type','file')
				a.setAttribute('accept', '.jpg,.jpeg,.png,.tif,.gif,.svg')
				a.click()
				a.onchange = function(){
					file = a.files[0];
					$(CKEDITOR.currentInstance).trigger('enableFormSubmit')
					curr = CKEDITOR.currentInstance
					if (file.size > 167772160){
						b = document.createElement('div')
						b.className = 'message alert alert-danger'
						m = document.createElement('span')
						//m.innerHTML = langs[lang]['warning-big-image']
                        alert('Image size cannot exceed 20 mb');
						b.appendChild(m);
						c = document.createElement('span')
						c.className = 'close'
						c.innerHTML = 'X'
						b.appendChild(c)
						e = document.querySelector('.error-space')
						e.appendChild(b)
						setTimeout(function(){
							alert = document.querySelector('.alert-danger')
							alert.parentNode.removeChild(alert)
						}, 20000)
						c.onclick = function(){
							b = document.querySelector('.alert-danger')
							b.parentNode.removeChild(b)
						}
						$(CKEDITOR.instances[curr.name]).trigger('enableFormSubmit')
						return
					}else if (['jpeg','jpg','png','svg','gif','tif', 'svg+xml'].indexOf(file.type.split('/')[1]) === -1){
						b = document.createElement('div')
						b.className = 'message alert alert-danger'
						m = document.createElement('span')
						//m.innerHTML = langs[lang]['warning-format-image']
                        alert('Valid file extensions: \'jpeg\',\'jpg\',\'png\',\'svg\',\'gif\',\'tif\', \'svg+xml\'');
						b.appendChild(m)
						c = document.createElement('span')
						c.className = 'close'
						c.innerHTML = 'X'
						b.appendChild(c)
						e = document.querySelector('.error-space')
						e.appendChild(b)
						setTimeout(function(){
							alert = document.querySelector('.alert-danger')
							alert.parentNode.removeChild(alert)
						}, 20000)
						c.onclick = function(){
							b = document.querySelector('.alert-danger')
							b.parentNode.removeChild(b)
						}
						$(CKEDITOR.instances[curr.name]).trigger('enableFormSubmit')
						return
					}
					img = new Image()
					img.onload = function(){
						inputWidth = this.width
						inputHeight = this.height
					}
					img.src = window.URL.createObjectURL(file)
					formData = new FormData;
					formData.append('file', file);
					loaderElem = new CKEDITOR.dom.element('loader-elem')
					loaderHtmlStr = '<div style="position: relative; z-index: 100;width: 100%;height: 100%;text-align: center;background: white;opacity: 0.75;pointer-events:none">' + '<div style="width: 100%;height: 30px;margin-top: 100px;">' + 'image-loading' + '</div>' + '</div>'
					loaderDomEle = CKEDITOR.dom.element.createFromHtml(loaderHtmlStr)
					loaderElem.append(loaderDomEle)
					editor.insertElement(loaderElem)
					CKEDITOR.currentInstance.setReadOnly(true);
					// my code
					let token = window.APP.$store.state.auth.auth.token;
					$.ajax({
						url: editor.config.imageUploadURL,
						type: 'POST',
						data: formData,
						processData: false,
						contentType: false,
						headers: {
							'Authorization': `${token.token_type} ${token.access_token}`
						},
                        success: (data, textStatus, jqXHR) => {
                            var isNew;
                            /*data = JSON.parse(data)*/
                            if (jqXHR.status == 200) {
                                widthBase = 655;
                                CKEDITOR.instances[curr.name].setReadOnly(false)
                                url = editor.config.dataParser(data)
                                maxWidth = Math.min(inputWidth, 600)
                                maxHeight = Math.min(inputHeight, 600)
                                if ((maxWidth/maxHeight) > (inputWidth/inputHeight)){
                                    width = (maxWidth * inputWidth)/inputHeight
                                    height = maxHeight
                                } else if ((maxWidth/maxHeight) < (inputWidth/inputHeight)){
                                    width = maxWidth
                                    height = (maxHeight * inputHeight)/inputWidth
                                } else{
                                    width = maxWidth
                                    height = maxHeight
                                }
                                if (editor.config.srcSet){
                                    srcSet = editor.config.srcSet(data)
                                    imgElem = '<img src="' + url + '" class="image-editor" srcset="'+ srcSet +'" data-width="' + inputWidth + '" data-height="' + inputHeight + '" height="' + height + '" width="' + width + '">'
                                } else{
                                    imgElem = '<img src="' + url + '" class="image-editor" data-width="' + inputWidth + '" data-height="' + inputHeight + '" height="' + height + '" width="' + width + '">'
                                }
                                imgDomElem = CKEDITOR.dom.element.createFromHtml(imgElem)
                                editor.insertElement(imgDomElem)
                                loaderElem.remove()
                                $(CKEDITOR.instances[curr.name]).trigger('enableFormSubmit')
                            }
                        },
                        error: (data, textStatus, jqXHR) => {
                            CKEDITOR.instances[curr.name].setReadOnly(false)
                            b = document.createElement('div')
                            b.className = 'message alert alert-danger'
                            m = document.createElement('span')
                            //m.innerHTML = langs[lang]['warning-error-image']
                            alert('Error while uploading file');
                            m.innerHTML ='warning-error-image'
                            b.appendChild(m)
                            c = document.createElement('span')
                            c.className = 'close'
                            c.innerHTML = 'X'
                            b.appendChild(c)
                            e = document.querySelector('.error-space')
                            try {
                                e.appendChild(b)
                            } catch (e) {
                                console.log(e);
                            }

                            loaderElem.remove()
                            $(CKEDITOR.instances[curr.name]).trigger('enableFormSubmit')
                            setTimeout(function(){
                                alert = document.querySelector('.alert-danger')
                                alert.parentNode.removeChild(alert)
                            }, 20000)
                            c.onclick = function(){
                                b = document.querySelector('.alert-danger')
                                b.parentNode.removeChild(b)
                            }
                        },
					})
				}

			}
		});

		editor.ui.addButton( 'SImage', {
			label: 'image',
			command: 'simage',
			toolbar: 'insert'
		});
	}
});
