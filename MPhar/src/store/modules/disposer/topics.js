import { get } from '@/utils/http-helper';

const GET_TOPICS = '/topic/index?size=10';
const GET_TOPICS_SHORT = '/topic/index-short';
const TOPIC_VIEW = '/topic/view';
const GET_PARTICIPANTS = '/participant/index-short';

export default {
  namespaced: true,
  state: {
      topicsInfo: null,
      topics: null,
      topicsShort: null,
      topic: null,
      participants: {},
      page: 1,
  },
  actions: {
    async get({ commit, state }, { id, page, size=0 }) {
        let newPage = page;
        if (!page) {
            newPage = state.page;
        }
        const topics = await get(GET_TOPICS, { event_id: id, page: newPage, size });
        if (topics.status) {
          commit('updateTopics', { topics: topics.data, page });
        }
    },
    async getTopic({ commit }, { topicId, eventId }) {
      const topic = await get(TOPIC_VIEW, { event_id: eventId, id: topicId });
      if (topic.status) {
        commit('updateTopic', topic.data);
        commit('updateParticipants', topic.data);
      }
    },
    async getTopicsShort({ commit }, { id, type }) {
      const topics = await get(GET_TOPICS_SHORT, { event_id: id, type });
      if (topics.status) {
        commit('updateTopicsShort', topics.data);
      }
    },
    async clearEditedTopic({ commit }) {
      commit('updateTopic', null);
    },
    async getParticipants({ commit }, { id, role }) {
        const participants = await get(GET_PARTICIPANTS, { event_id: id, role: role });
        if (participants.status) {
          commit('updateParticipants', participants.data);
        }
    },
  },
  mutations: {
      updateTopics(state, { topics, page }) {
          state.topicsInfo = topics;
          state.topics = topics.topics;
          if (page) {
              state.page = page;
          }
      },
      updateTopicsShort(state, topics) {
          state.topicsShort = topics.topics;
      },
      updateTopic(state, topic) {
        state.topic = topic;
      },
      updateParticipants(state, participants) {
          state.participants = participants.participants;
      },
  },
  getters: {
      get: state => state.topics,
      getTopicsInfo: state => state.topicsInfo,
      getParticipants: state => state.participants,
      getTopic: state => state.topic,
      getTopicsShort: state => state.topicsShort,
      getPage: state => state.page,
  },
};
