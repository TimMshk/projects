import { get } from '@/utils/http-helper';

const GET_LIBRARY = '/library/get';

export default {
  state: {
    library: null,
  },
  actions: {
    async getLibrary({ commit }, eventId) {
      const library = await get(GET_LIBRARY, { event_id: eventId });
      if (library.status) {
        commit('updateLibrary', library.data);
      }
    },
  },
  mutations: {
    updateLibrary(state, library) {
      state.library = library;
    },
  },
  getters: {
    getLibrary: state => state.library,
  },
};
