import { get } from '@/utils/http-helper';

const GET = '/workshop/get';
const GET_REVIEW = '/review/get';

export default {
  namespaced: true,
  state: {
    workshop: null,
  },
  actions: {
    async get({ commit }, topicId) {
      const workshop = await get(GET, { topic_id: topicId });
      if (workshop.status) {
        commit('updateWorkshop', workshop.data);
      }
    },
    async getReview({ commit }, topicId) {
      const workshop = await get(GET_REVIEW, { topic_id: topicId });
      if (workshop.status) {
        commit('updateWorkshop', workshop.data);
      }
    },
    async getPage({ commit }, { topicId, page , path }) {
      const workshop = await get(path, { topic_id: topicId, page });
      if (workshop.status) {
        commit('updateWorkshop', workshop.data);
      }
    },
  },
  mutations: {
    updateWorkshop(state, workshop) {
      state.workshop = workshop;
    },
  },
  getters: {
    get: state => state.workshop,
  },
};
