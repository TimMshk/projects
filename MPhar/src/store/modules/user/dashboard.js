import Vue from 'vue';
import { get, post } from '@/utils/http-helper';

const GET_DASHBOARD = '/dashboard/get';
const GET_DASHBOARD_AGENDA = '/dashboard/get-agenda';
const TRANSLATE = '/dashboard/translate';


const findComment = (arr, id, questionId) => {
    let comment;

    let container = arr.comments;

    if (questionId) {
      container = arr.questions.find((q) => {
        return +q.id === +questionId;
      }).comments;
    }

    container = container.reduce((total, c)=>{
        return [...total, c, ...c.comments];
    }, []);

    comment = container.find((c) => {
      return +c.id === +id;
    });

    return comment;
};

export default {
  namespaced: true,

  state: {
      dashboard: null,
      agenda: null,
  },

  actions: {
    async get({ commit }, params) {
      const dashboard = await get(GET_DASHBOARD, { id: params.id, event_id: params.eventId });
      if (dashboard?.data) {
        commit('updateDashboard', dashboard.data);
      }
    },
    async getAgenda({ commit }, eventId) {
      const agenda = await get(GET_DASHBOARD_AGENDA, { event_id: eventId });
      if (agenda?.data) {
        commit('updateAgenda', agenda.data);
      }
    },
    changeLikesCount({ commit }, id) {
      commit('changeLikesCount', id);
    },
    createComment({ commit, rootState }, data) {
        commit('createComment', data);
        if (data.question_id && rootState.auth.user.id !== data.user_id) {
            commit('readQuestion', { id: data.question_id, status: true });
        }
    },
    toggleCommentState({ commit }, data) {
        commit('toggleCommentState', data);
    },
    answerSurvey({ commit }, data) {
        commit('answerSurvey', data);
    },
    readQuestion({ commit }, data) {
        commit('readQuestion', data);
    },
    async translateTopic({ commit }, data) {
      const translations = await post(TRANSLATE, data);
      commit('translate', translations.data);
    },
  },

  mutations: {
      updateDashboard(state, dashboard) {
        if (dashboard.comments) {
            dashboard.comments.forEach((c)=>c.showComments = false);
        } else if (dashboard.questions) {
            dashboard.questions.forEach((q) => {
              q.comments.forEach((c) => {
                  c.showComments = false;
              });
            });
        }
        if (dashboard.surveys) {
            dashboard.surveys.forEach((s) => {
                s.options?.forEach((o) => {
                    o.inputValue = null;
                });
            });
        }
        state.dashboard = dashboard;
      },
      createComment(state, data) {
          const comment = {
              id: data.id,
              avatar: data.avatar,
              date: data.created_at,
              content: data.content,
              name: data.author,
              comments: [],
              count_likes: 0,
              liked: 0,
              count_comments: 0,
              deleted: 0,
              extra: data.extra,
              type: data.type,
              user_id: data.user_id,
              showComments: false,
          };

          let container = state.dashboard;

          if (data.question_id) {
              container = state.dashboard.questions.find((q) => {
                  return +q.id === +data.question_id;
              });
          }

          if (data.parent_id) {
            container = container.comments.find((c) => {
              return +c.id === +data.parent_id;
            });
          }

          container.count_comments ++;
          container.showComments = true;
          !data.parent_id? container.comments.unshift(comment): container.comments.push(comment);
      },
      changeLikesCount(state, { id, up, self=false, questionId }) {
          const comment = findComment(state.dashboard, id, questionId);

          if (up) {
            comment.count_likes ++;
            self? comment.liked = 1: null;
          } else  {
            comment.count_likes --;
            self? comment.liked = 0: null;
          }
      },
      toggleCommentState(state, data) {
          const comment = findComment(state.dashboard, data.comment_id, data.question_id);
          comment.deleted = data.deleted;
          comment.content = data.content;
      },
      readQuestion(state, { id, status }) {
          const question = state.dashboard.questions.find(q => +q.id === +id);
          question.is_unread = status;
      },
      answerSurvey(state, data) {
          const dashboard = { ...state.dashboard };
          if (!data.answers) {
              let surveys = dashboard.surveys;
              if (!surveys) {
                  const question = dashboard.questions.find(q => +q.id === +data.question_id);
                  surveys = question.surveys;
                  question.surveys = surveys.filter(s => +s.id !== +data.survey_id);
              } else  {
                  dashboard.surveys = surveys.filter(s => +s.id !== +data.survey_id);
              }
              state.dashboard = { ...dashboard };
              return;
          }
          console.log(data);

         let survey = state.dashboard?.surveys?.find(s => +s.id === +data.survey_id);

         if (!survey && data.question_id) {
           const question = state.dashboard.questions.find(q => +q.id === + data.question_id);
           survey = question.surveys.find(s => +s.id === +data.survey_id);
           setTimeout(()=>{
               const unansweredSurvey = question.surveys.find(s => !s.answers);
               if (!unansweredSurvey) {
                   question.is_answered = 1;
                   state.dashboard = { ...dashboard };
               }
           });
         }
         let answers = data.answers;
          let advanced = null;
         if (survey.advanced) {
           advanced = { ...survey.advanced };
           advanced.comments = [...advanced.comments, ...data.advanced.comments];
         }
         if (data.survey_id === 4) {
            answers = survey.answers;
            answers.total += data.comments.values.length;
            data.comments.values.forEach((c) => {
                const variant = answers.values.find(v => +v.id === +c.id);
                variant.comments.push({
                  name: data.comments.author,
                  content: c.value,
                });
            });
         }
          Vue.set(survey, 'answers', answers);
          Vue.set(survey, 'advanced', advanced);
      },
      updateAgenda(state, agenda) {
        state.agenda = agenda;
      },
      translate(state, translations) {
        let comments = state.dashboard.comments;
        if (state.dashboard.questions) {
          translations = translations[0];
          comments = state.dashboard.questions;
          const question = comments.find(c => +c.id === +translations.id);
          question.intro = translations.intro;
          question.name = translations.name;
          question.subtitle = translations.subtitle;
          changeContent(question.comments);
        } else {
          changeContent(comments);
        }

        function changeContent(comments) {
          translations.comments.forEach(c => {
            let comment = null;
            comments.forEach(cc => {
              if (+cc.id === +c.id) {
                comment = cc;
              } else if (cc.comments){
                const com = cc.comments.find(ccc => +ccc.id === +c.id);
                if (com) {
                  comment = com;
                }
              }
            });
            comment.content = c.content;
          });
        }
      },
  },

  getters: {
      get: state => state.dashboard,
      topicId: state => state.dashboard?.id,
      getAgenda: state => state.agenda,
  },
};

