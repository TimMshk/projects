import { get } from '@/utils/http-helper';

const GET_PARTICIPANTS = '/participant/get';

export default {
  state: {
    participants: null,
  },
  actions: {
    async getParticipants({ commit }, eventId) {
      const participants = await get(GET_PARTICIPANTS, { event_id: eventId });
      if (participants.status) {
        commit('updateParticipants', participants.data);
      }
    },
  },
  mutations: {
    updateParticipants(state, participants) {
      state.participants = participants;
    },
  },
  getters: {
    getParticipants: state => state.participants,
  },
};
