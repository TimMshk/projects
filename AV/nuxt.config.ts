import { existsSync, readFileSync } from 'fs';
import { resolve } from 'path';
// @ts-ignore
import { parse } from 'dotenv';

if (process.env.NODE_ENV !== 'production') {
    const env = resolve(__dirname, '.env');
    const envExample = resolve(__dirname, '.env.example');
    if (!existsSync(env) || !existsSync(envExample)) throw new Error('One of environment files (.env or .env.example) does not exist');

    const envParsed = Object.keys(parse(readFileSync(env, 'utf-8')));
    const envExampleParsed = Object.keys(parse(readFileSync(envExample, 'utf-8')));

    if (Object.keys(envParsed).length !== Object.keys(envExampleParsed).length) {
        let error = 'File .env differs from file .env.example: variables mismatch';

        for (const key of envParsed) {
            if (!envExampleParsed.includes(key)) error += `\n${ key } key is present in .env, but missing in .env.example`;
        }
        for (const key of envExampleParsed) {
            if (!envParsed.includes(key)) error += `\n${ key } key is present in .env.example, but missing in .env`;
        }

        throw new Error(error);
    }
}

const packageJSON = require('./package.json');

const BASE_URL = process.env.BASE_URL;
const BROWSER_BASE_URL = process.env.BROWSER_BASE_URL;

export default {
    ssr: true,
    target: 'server',
    globalName: 'app',
    env: {
        gtagParam: process.env.GTAG_PARAM,
    }, // common env
    publicRuntimeConfig: {
        API_URL: BASE_URL,
        BROWSER_API_URL: BROWSER_BASE_URL,
        axios: {
            browserBaseURL: BROWSER_BASE_URL,
        },
    }, // client-side env https://ru.nuxtjs.org/guide/runtime-config/
    privateRuntimeConfig: {
        API_URL: BASE_URL,
        BROWSER_API_URL: BROWSER_BASE_URL,
        axios: {
            browserBaseURL: BROWSER_BASE_URL,
        },
    }, // server-side env https://ru.nuxtjs.org/guide/runtime-config/

    alias: {
        '@': resolve(__dirname, './src/'),
    },
    messages: {
        nuxtjs: '',
        loading: 'Загрузка...',
        error_404: 'Страница не найдена',
        server_error: 'Ошибка сервера',
        back_to_home: 'Вернуться на главную',
        server_error_details: 'В приложении произошла ошибка, и оно не может быть отображено. Если вы сотрудник Азбуки Вкуса, проверьте логи приложения.',
        client_error: 'Ошибка',
        client_error_details: 'Произошла ошибка при обработке этой страницы. Проверьте логи консоли.',
    },
    watchers: {
        webpack: {
            poll: true,
        },
    },
    head: {
        title: 'av-b2b',
        htmlAttrs: {
            lang: 'ru',
        },
        meta: [
            { charset: 'utf-8' },
            {
                'http-equiv': 'X-UA-Compatible',
                content: 'IE=edge',
            },
            {
                name: 'viewport',
                content: 'width=device-width, initial-scale=1, maximum-scale=1',
            },
        ],
        link: [
            {
                rel: 'icon',
                type: 'image/x-icon',
                sizes: 'any',
                href: '/favIcons/favicon.ico',
            },
            {
                rel: 'apple-touch-icon',
                href: '/favIcons/apple-touch-icon.png',
            },
            {
                rel: 'manifest',
                href: '/favIcons/site.webmanifest',
            },
        ],
        script: [
            { src: 'https://api-maps.yandex.ru/2.1/?apikey=e8a49792-9630-4b69-a90e-01729b902231&lang=ru_RU' },
            { src: 'https://www.googletagmanager.com/gtag/js?id=UA-192004005-3' },
        ],
        /* Вставка тэгов в meta script/noscript запрещено по умолчанию. Командой ниже это можно разрешить
         __dangerouslyDisableSanitizers: ['script', 'noscript'], */
    },

    plugins: [
        {
            src: '~/src/plugins/ssr.ts',
            ssr: true,
        },
        {
            src: '~/src/plugins/client.ts',
            ssr: false,
        },
        {
            src: '~/src/plugins/http-helper.ts',
        },
        {
            src: '~/src/plugins/dayjs.ts',
        },
        {
            src: '~/src/plugins/vMask.ts',
        },
        {
            src: '~/src/plugins/vue-scrollto.ts',
            ssr: false,
        },
        {
            src: '~/src/plugins/vue-slider-component.ts',
            ssr: false,
        },
        {
            src: '~/src/plugins/vuelidate.ts',
        },
        {
            src: '~/src/plugins/vue-scroll-lock.ts',
            ssr: false,
        },
        {
            src: '~/src/plugins/v-click-outside.ts',
            ssr: false,
        },
        {
            src: '~/src/plugins/vue-simple-calendar.ts',
            ssr: false,
        },
        {
            src: '~/src/plugins/v-tooltip.ts',
        },
        {
            src: '~/src/plugins/ga.ts',
            mode: 'client',
        },
    ],

    // Авто-импорт компонентов: https://go.nuxtjs.dev/config-components
    components: false,

    rootDir: __dirname,
    styleResources: {
        scss: [
            './src/assets/scss/abstracts/variables.scss',
            './src/assets/scss/abstracts/mixins.scss',
        ],
    },
    // Нужно для авто релоада
    css: [
        {
            src: '~src/assets/scss/main.scss',
        },
    ],

    // Модули для разработки и сборки: https://go.nuxtjs.dev/config-modules
    buildModules: [
        [
            '@nuxt/typescript-build',
            {
                typeCheck: {
                    eslint: {
                        files: './**/*.{js,ts,vue}',
                    },
                    typescript: {
                        extensions: {
                            vue: true,
                        },
                    },
                },
            },
        ],
        ['@nuxtjs/stylelint-module', {
            configFile: 'stylelint.js',
        }],
        '@nuxtjs/style-resources',
        'nuxt-typed-vuex',
        '@nuxtjs/dayjs',
    ],

    dayjs: {
        locales: ['ru'],
        defaultLocale: 'ru',
        plugins: [
            'updateLocale',
            'isBetween',
        ],
    },

    // Modules: https://go.nuxtjs.dev/config-modules
    modules: [
        '@nuxtjs/sentry',
        //Используйте с осторожностью. https://ssr.vuejs.org/guide/caching.html#component-level-caching
        //['@nuxtjs/component-cache', { maxAge: process.env.NODE_ENV === 'development' ? 0 : 1000 * 60 * 15 }],
        ['cookie-universal-nuxt', { parseJSON: true }],
        'nuxt-route-meta',
        '@nuxtjs/axios',
        //https://pwa.nuxtjs.org/
        //['@nuxtjs/pwa', {}]
    ],

    axios: {
        baseURL: BASE_URL,
    },

    sentry: {
        sourceMapStyle: 'hidden-source-map',
        config: {
            environment: process.env.SENTRY_ENVIRONMENT || undefined,
            release: process.env.SENTRY_DSN ? `frontend-${ packageJSON.version }` : undefined,
        },
        /**
         * Options passed to @sentry/webpack-plugin. See documentation at
         * https://github.com/getsentry/sentry-webpack-plugin/blob/master/README.md
         */
        webpackConfig: {},
        tracing: false,
        /*tracing: {
         tracesSampleRate: 1.0,
         vueOptions: {
         tracing: true,
         tracingOptions: {
         hooks: ['mount', 'update'],
         timeout: 2000,
         trackComponents: true,
         },
         },
         browserOptions: {},
         },*/
    },

    // PWA module configuration: https://go.nuxtjs.dev/pwa
    /* pwa: {
     manifest: {
     lang: 'ru',
     },
     }, */

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {
        corejs: 3,
        // eslint-disable-line no-shadow
        extend(config: any) {
            const svgRule = config.module.rules.find((rule: any) => rule.test.test('.svg'));

            svgRule.test = /\.(png|jpe?g|gif|webp)$/i;

            config.module.rules.push({
                test: /\.svg$/,
                oneOf: [
                    {
                        resourceQuery: /inline/,
                        use: [
                            'vue-loader',
                            'vue-svg-loader',
                        ],
                    },
                    // Add original url-loader config to resolve every other svg without inline query
                    svgRule.use[0],
                ],
            });

            return config;
        },
        filenames: {
            app: ({
                isDev,
                isModern,
            }: any) => isDev
                ? `[name].js`
                : `chunks/[id]-entrypoint${ isModern
                    ? '-modern'
                    : '' }.[contenthash].js`,
            chunk: ({
                isDev,
                isModern,
            }: any) => isDev
                ? `[name].js`
                : `chunks/[id]${ isModern
                    ? '-modern'
                    : '' }.[contenthash].js`,
            css: ({ isDev }: any) => isDev ? '[name].css' : 'css/[id].[contenthash].css',
            img: ({ isDev }: any) => isDev ? '[path][name].[ext]' : 'images/[contenthash:7].[ext]',
            font: ({ isDev }: any) => isDev ? '[path][name].[ext]' : 'fonts/[contenthash:7].[ext]',
            video: ({ isDev }: any) => isDev ? '[path][name].[ext]' : 'videos/[contenthash:7].[ext]',
        },
        devtools: process.env.NODE_ENV === 'development',
        publicPath: '/static/',
        extractCSS: process.env.NODE_ENV !== 'development',
        transpile: ['typed-vuex'],
    },
    server: {
        port: process.env.PORT,
        host: '0.0.0.0',

        https: false,
    },
    dir: {
        assets: 'src/assets',
        app: 'src/app',
        layouts: 'src/layouts',
        middleware: 'src/middleware',
        pages: 'src/pages',
        static: 'public',
        store: 'src/store',
    },
    loading: {
        color: '#72AE35',
        failedColor: '#E40043',
        height: '3px',
    },
    cli: {
        badgeMessages: ['Azbuka Vkusa project'],
    },
    router: {
        base: process.env.BASE_ROUTE || '/',
        middleware: [],
        mode: 'history', //history или hash
        //trailingSlash: false,
    },
    modern: process.env.NODE_ENV !== 'development' ? 'server' : false,
    telemetry: false,
};
