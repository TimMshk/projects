ARG NODE_BASE_IMAGE="node:14.15.5-slim"


FROM ${NODE_BASE_IMAGE} as base
# Add Tini
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini /tini
RUN chmod +x /tini
ENTRYPOINT ["/tini", "--"]
# Basic settings
RUN mkdir -p /app && chown -R node:node /app
WORKDIR /app
USER node
COPY --chown=node:node package.json package-lock.json yarn.lock ./
ARG NPM_TOKEN
ARG NPM_MISC_REGISTRY="gitlab.com/api/v4/projects/28735553"
RUN npm config set @misc:registry https://${NPM_MISC_REGISTRY}/packages/npm/ \
    && npm config set //${NPM_MISC_REGISTRY}/packages/npm/:_authToken "${NPM_TOKEN}"


FROM base as dev
RUN yarn install --frozen-lockfile
ENV PATH=/app/node_modules/.bin:$PATH
CMD ( [ -d "node_modules" ] || ( echo "Restore node_modules..." && yarn install --frozen-lockfile ) ) && yarn dev


FROM base as prod
RUN yarn install --frozen-lockfile
COPY --chown=node:node . .
ENV NODE_ENV=production
RUN yarn build
ENV PATH=/app/node_modules/.bin:$PATH
CMD ["node", "node_modules/nuxt/bin/nuxt.js", "start"]
