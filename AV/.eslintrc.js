module.exports = {
    root: true,
    env: {
        browser: true,
        node: true,
        commonjs: true,
    },
    extends: [
        'eslint:recommended',
        '@nuxtjs/eslint-config-typescript',
        'plugin:vue/recommended',
        'plugin:nuxt/recommended',
    ],
    rules: {
        'import/order': 'off',
        semi: [
            'error',
            'always',
        ],
        'object-curly-spacing': ['warn', 'always'],
        curly: [
            'error',
            'multi-line',
        ],
        'template-curly-spacing': [
            'warn',
            'always',
        ],
        'keyword-spacing': 'warn',
        quotes: [
            'error',
            'single',
            {
                allowTemplateLiterals: true,
                avoidEscape: true,
            },
        ],
        'brace-style': [
            'error',
            'stroustrup',
        ],
        'no-multi-spaces': [
            'error',
            {
                exceptions: {
                    ImportDeclaration: true,
                },
            },
        ],
        '@typescript-eslint/no-unused-vars': [
            'error',
            {
                args: 'none',
                caughtErrorsIgnorePattern: '^ignore',
                vars: 'local',
            },
        ],
        'comma-dangle': [
            'error',
            'always-multiline',
        ],
        indent: [
            'error',
            4,
            {
                SwitchCase: 1,
            },
        ],
        '@typescript-eslint/no-var-requires': 'off',
        '@typescript-eslint/no-explicit-any': 'off',
        'no-useless-constructor': 'off',
        '@typescript-eslint/no-useless-constructor': [
            'error',
        ],
        'spaced-comment': 'off',
        'space-before-function-paren': 'off',
        'no-console': 'off',
        'no-debugger': 'off',
        'prefer-const': [
            'error',
            {
                destructuring: 'all',
            },
        ],
        camelcase: [
            'off',
        ],
        '@typescript-eslint/naming-convention': [
            'error',
            {
                selector: 'default',
                format: [
                    'camelCase',
                    'PascalCase',
                    'UPPER_CASE',
                ],
                leadingUnderscore: 'allow',
                trailingUnderscore: 'allow',
            },
            {
                selector: 'class',
                format: [
                    'PascalCase',
                ],
            },
            {
                selector: 'variable',
                modifiers: [
                    'destructured',
                ],
                format: null,
            },
            {
                selector: 'typeParameter',
                format: null,
            },
            {
                selector: 'enumMember',
                format: null,
            },
            {
                selector: 'interface',
                format: null,
            },
            {
                selector: 'property',
                format: null,
            },
        ],
        '@typescript-eslint/no-redeclare': [
            'error',
        ],
        'vue/component-name-in-template-casing': [
            'error',
            'kebab-case',
        ],
        'vue/script-indent': [
            'warn',
            4,
            {
                baseIndent: 0,
                switchCase: 1,
                ignores: [],
            },
        ],
        'vue/html-indent': [
            'warn',
            4,
        ],
        'vue/max-attributes-per-line': [
            'error', {
                singleline: 1,
            },
        ],
        'vue/padding-line-between-blocks': ['error', 'always'],
        'vue/html-closing-bracket-newline': [
            'error',
            {
                singleline: 'never',
                multiline: 'always',
            },
        ],
        'vue/no-unused-components': 'warn',
        'vue/no-unused-vars': 'warn',
        'vue/no-v-html': ['off'],
        'vue/no-template-shadow': ['off'],
        'vue/valid-v-slot': [
            'error',
            {
                allowModifiers: true,
            },
        ],
        'vue/html-self-closing': [
            'error', {
                html: {
                    normal: 'never',
                    void: 'always',
                },
            },
        ],
    },
    overrides: [
        {
            files: ['*.vue'],
            rules: {
                indent: 'off',
            },
        },
    ],
};
