import { Middleware } from '@nuxt/types';

const notAuthenticated: Middleware = (context) => {
    if (context.app.$cookies.get('accessToken')) {
        context.redirect('/');
    }
};

export default notAuthenticated;
