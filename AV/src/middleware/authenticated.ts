import { Middleware } from '@nuxt/types';

const authenticated: Middleware = (context) => {
    if (!context.app.$cookies.get('accessToken')) {
        context.redirect('/login');
    }
};

export default authenticated;
