import { getterTree, mutationTree, actionTree } from 'typed-vuex';

import { ICartItem, IDeliveryInfo, IOrdering } from '~/src/utils/interfaces/cart';

import {
    RGetDeliveryPrice,
    RGetCart,
    RUpdateQuantity, ROverwriteCart,
} from '~/src/utils/api/requests/cart';
import { mergeCart } from '~/src/utils/helpers/cart';

export const state = () => ({
    cartProducts: [] as ICartItem[],
    cartPrice: 0,
    isCartLoading: true,
    ordering: {} as IOrdering,
    deliveryInfo: {} as IDeliveryInfo,
    deliveryPrice: 0,
});

export const getters = getterTree(state, {
    cart(state): ICartItem[] {
        return state.cartProducts;
    },

    actualCart(state): ICartItem[] {
        return state.cartProducts.filter((product: ICartItem) => !product.save);
    },

    cartPrice(state): number {
        return state.cartPrice;
    },

    deliveryInfo(state): IDeliveryInfo {
        return state.deliveryInfo;
    },

    loadingCart(state): boolean {
        return state.isCartLoading;
    },

    deliveryPrice(state): number {
        return state.deliveryPrice;
    },

    ordering(state): IOrdering {
        return state.ordering;
    },
});

export const mutations = mutationTree(state, {
    addProductInCart(state, { product, quantity, token, save }) {
        let cart = [...state.cartProducts];
        const productPrice = product.unitPrice * quantity;
        const newProduct: ICartItem = {
            product,
            quantity,
            save,
            price: productPrice,
        };

        if (quantity) {
            const cartItem = cart.find(cartItem => cartItem.product.id === product.id);
            if (cartItem && quantity) {
                cartItem.quantity = quantity;
                cartItem.price = productPrice;
            }
            else {
                cart.push(newProduct);
            }
        }
        else {
            cart = cart.filter(cartItem => cartItem.product.id !== product.id);
        }
        state.cartProducts = [...cart];

        if (!token) {
            localStorage.setItem('cart', JSON.stringify(state.cartProducts));
        }
    },

    setCartPrice(state, price?: number) {
        if (price) {
            state.cartPrice = price;
        }
        else {
            state.cartPrice =
                +state.cartProducts.reduce((total: number, product: ICartItem) => !product.save ? total + product.product.unitPrice * product.quantity : total, 0);
        }
    },

    addProductInCartWithSave(state, { product, token, save }) {
        const cart = [...state.cartProducts];

        const cartItem = cart.find(cartItem => cartItem.product.id === product.id);
        cartItem!.save = !!save;

        if (!token) {
            const filteredCart = cart.filter(cartItem => !cartItem.save);
            localStorage.setItem('cart', JSON.stringify(filteredCart));
        }

        state.cartProducts = JSON.parse(JSON.stringify(cart));
    },

    setCart(state, cart: ICartItem[]) {
        if (state.cartProducts.length) {
            const cartProducts = state.cartProducts.filter((cartItem: ICartItem) =>
                cart.find((newCartItem: ICartItem) => cartItem.product.id === newCartItem.product.id) || cartItem.save);

            const newCart = cart.reduce((result, newCartItem: ICartItem) => {
                const sampleIndex = result.findIndex((cartItem: ICartItem) => cartItem.product.id === newCartItem.product.id);
                if (sampleIndex >= 0) {
                    result[sampleIndex] = newCartItem;
                }
                else {
                    result.push(newCartItem);
                }
                return result;
            }, cartProducts);
            state.cartProducts = newCart;
        }
        else {
            state.cartProducts = cart;
        }
    },

    setCartOrdering(state, ordering: IOrdering) {
        state.ordering = ordering;
    },

    clearCart(state) {
        state.cartProducts = [];
        localStorage.removeItem('cart');
    },

    setDeliveryInfo(state, info: IDeliveryInfo) {
        state.deliveryInfo = info;
    },

    setDeliveryPrice(state, price: number) {
        state.deliveryPrice = price;
    },

    setLoadingStatus(state, status) {
        state.isCartLoading = status;
    },
});

export const actions = actionTree({ state, mutations, getters }, {
    async getCartAction({ commit, dispatch, rootState }) {
        commit('setLoadingStatus', true);
        const token = rootState.auth.accessToken;
        if (token) {
            const response = await this.$request(RGetCart());
            if (response.RESPONSE_STATUS === 200) {
                commit('setCart', response.cartProducts);
                commit('setCartOrdering', response.ordering);
                commit('setCartPrice', response.totalPrice);
                commit('setDeliveryPrice', response.deliveryPrice);
            }
        }
        else {
            commit('setCart', JSON.parse(String(localStorage.getItem('cart'))) || []);
            // @ts-ignore
            commit('setCartPrice');
        }
        commit('setLoadingStatus', false);
    },

    async changeCartAction({ commit, dispatch, rootState }, product: ICartItem) {
        const token = rootState.auth.accessToken;
        if (token) {
            const response = await this.$request(RUpdateQuantity({ product }));
            if (response.RESPONSE_STATUS === 200) {
                commit('setCart', response.cartProducts);
                commit('setCartOrdering', response.ordering);
                commit('setCartPrice', response.totalPrice);
                commit('setDeliveryPrice', response.deliveryPrice);
            }
        }
        if (!token) {
            commit('addProductInCart', { ...product, token });
            // @ts-ignore
            commit('setCartPrice');
        }
    },

    async changeCartActionWithSave({ commit, dispatch, rootState }, product: ICartItem) {
        const token = rootState.auth.accessToken;
        if (token) {
            const response = await this.$request(RUpdateQuantity({ product }));
            if (response.RESPONSE_STATUS === 200) {
                commit('setCartPrice', response.totalPrice);
                commit('setCartOrdering', response.ordering);
            }
        }
        commit('addProductInCartWithSave', { ...product, token });
        if (!token) {
            // @ts-ignore
            commit('setCartPrice');
        }
    },

    async mergeCart({ rootState, commit }) {
        const token = rootState.auth.accessToken;
        if (token) {
            const response = await this.$request(RGetCart());
            let BDCart = [] as ICartItem[];
            if (response.RESPONSE_STATUS === 200) {
                BDCart = response.cartProducts;
            }

            const localCart = JSON.parse(String(localStorage.getItem('cart')));
            if (localCart) {
                const mergedCart = mergeCart(BDCart, localCart);
                const response = await this.$request(ROverwriteCart({ cart: mergedCart }));
                if (response.RESPONSE_STATUS === 200) {
                    commit('setCart', mergedCart);
                    localStorage.removeItem('cart');
                }
            }
        }
    },

    async clearCartAction({ commit, rootState }) {
        const token = rootState.auth.accessToken;
        if (token) {
            await this.$request(ROverwriteCart({ cart: [] }));
        }
        commit('clearCart');
    },

    setCartAction({ commit }, cart) {
        commit('setCart', cart);
    },

    setDeliveryInfoAction({ commit }, info: IDeliveryInfo) {
        commit('setDeliveryInfo', info);
    },

    async getDeliveryPriceAction({ commit }) {
        const response = await this.$request(RGetDeliveryPrice());
        if (response.RESPONSE_STATUS === 200) {
            commit('setDeliveryPrice', response.price);
        }
    },

});
