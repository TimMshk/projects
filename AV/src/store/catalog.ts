import { getterTree, mutationTree, actionTree } from 'typed-vuex';
import { IUserFavorite } from '~/src/utils/interfaces/auth';
import { ICategory, ICategories, IProductsItem, IProductsResponse, IProductsPagination } from '~/src/utils/interfaces/catalog';
import {
    RGetCategories, RGetProducts, RAddFavorite, RRemoveFavorite, RGetFavorites,
} from '~/src/utils/api/requests/catalog';

export const state = () => ({
    isLoading: true,

    categoriesList: [] as ICategory[],
    categoriesThree: [] as ICategory[],
    products: {} as IProductsResponse,
    favorites: [] as IUserFavorite[],

    categoryId: 0,
    filters: {},
    orderBy: 'popular',
    paginationPage: 1,
});

export const getters = getterTree(state, {
    categoriesList(state): ICategory[] {
        return state.categoriesList;
    },

    categories(state): ICategory[] {
        return state.categoriesThree;
    },

    productsList(state): IProductsItem[] {
        return state.products.items;
    },

    productsPagination(state): IProductsPagination {
        return state.products.pagination;
    },

    productsPaginationPage(state): number {
        return state.paginationPage;
    },

    favorites(state): IUserFavorite[] {
        return state.favorites;
    },

    loading(state): boolean {
        return state.isLoading;
    },

    filters(state): object {
        return state.filters;
    },
});

export const mutations = mutationTree(state, {
    setCategories(state, categories: ICategories) {
        state.categoriesList = categories.categories;

        state.categoriesThree = categories.categories.reduce((a: ICategory[], b: ICategory) => {
            b.children = categories.categories.filter((c: ICategory) => +c.treeParentId! === +b.id);
            a.push(b);
            return a;
        }, []).filter((i: ICategory) => +i.treeParentId! === +categories.treeRootId!);
    },

    setCategory(state, categoryId: number) {
        state.categoryId = categoryId;
    },

    setFilters(state, filters) {
        state.filters = {
            ...state.filters,
            ...filters,
        };
    },

    resetFilters(state) {
        state.filters = {};
    },

    setOrder(state, order: string) {
        state.orderBy = order;
    },

    setProducts(state, products: IProductsResponse) {
        state.paginationPage = 1;
        state.products = products;
    },

    addMoreProducts(state, products: IProductsResponse) {
        state.paginationPage++;
        state.products.items = [...state.products.items, ...products.items];
    },

    updateFavorites(state, favorites: IUserFavorite[]) {
        state.favorites = favorites;
    },

    addToFavorite(state, product) {
        state.favorites.push(product);
    },

    removeFavorite(state, id) {
        state.favorites = state.favorites.filter(product => product.id !== id);
    },

    setLoadingStatus(state, status) {
        state.isLoading = status;
    },
});

export const actions = actionTree({ state, mutations, getters }, {
    async getCategoriesAction({ commit }) {
        const categories = await this.$request(RGetCategories()) as ICategories;
        commit('setCategories', categories);
    },

    setCategoryAction ({ commit }, category: number) {
        commit('setCategory', category);
    },

    setFiltersAction ({ commit }, filters) {
        commit('setFilters', filters);
    },

    resetFiltersAction({ commit }) {
        commit('resetFilters');
    },

    setOrderAction({ commit }, order: string) {
        commit('setOrder', order);
    },

    async getProductsAction ({ commit, state, rootState }) {
        commit('setLoadingStatus', true);
        const requestObj = {
            requestObj: {
                categoryId: +state.categoryId,
                filter: state.filters,
                pagination: {
                    limit: 12,
                    page: 1,
                },
                orderBy: state.orderBy,
            },
        };
        const products = await this.$request(RGetProducts(requestObj)) as IProductsResponse;

        const hasAlcohol = products.items.find((product: IProductsItem) => product.featureNonStandard === 'alcohol');
        if (hasAlcohol && !rootState.ageConfirmed && !rootState.auth.accessToken) {
            //@ts-ignore
            this.$accessor.modal.OPEN_MODAL({ name: 'AlcoholicWarning', payload: { mode: 'ageGate' } });
        }

        commit('setProducts', products);
        commit('setLoadingStatus', false);
    },

    async getMoreProductsAction ({ commit, state, rootState }) {
        commit('setLoadingStatus', true);
        const requestObj = {
            requestObj: {
                categoryId: state.categoryId,
                filter: state.filters,
                pagination: {
                    limit: 12,
                    page: state.paginationPage + 1,
                },
                orderBy: state.orderBy,
            },
        };
        const products = await this.$request(RGetProducts(requestObj)) as IProductsResponse;

        const hasAlcohol = products.items.find((product: IProductsItem) => product.featureNonStandard === 'alcohol');
        if (hasAlcohol && !rootState.ageConfirmed && !rootState.auth.accessToken) {
            //@ts-ignore
            this.$accessor.modal.OPEN_MODAL({ name: 'AlcoholicWarning', payload: { mode: 'ageGate' } });
        }

        commit('addMoreProducts', products);
        commit('setLoadingStatus', false);
    },

    async getFavoritesAction({ commit, rootState }) {
        const token = rootState.auth.accessToken;
        if (token) {
            const response = await this.$request(RGetFavorites());
            if (response.RESPONSE_STATUS === 200) {
                commit('updateFavorites', response.favorites);
            }
        }
    },

    async addFavoriteAction({ commit }, props) {
        const response = await this.$request(RAddFavorite(props.requestObj.requestObj));

        if (response.RESPONSE_STATUS === 200) {
            commit('addToFavorite', props.product);
        }

        return response;
    },

    async removeFavoriteAction({ commit }, { requestObj }) {
        const response = await this.$request(RRemoveFavorite(requestObj));

        if (response.RESPONSE_STATUS === 200) {
            commit('removeFavorite', requestObj.productId);
        }

        return response;
    },
});
