import { actionTree, getAccessorType, getterTree, mutationTree } from 'typed-vuex';

import * as modal from './modal';
import * as prompt from './prompt';
import * as auth from './auth';
import * as catalog from './catalog';
import * as cart from './cart';

import { RGetSettings } from '~/src/utils/api/requests/auth';

export const strict = true;

export const state = () => ({
    apiEndpoint: null, //Сервер для запросов к API через дефолтную библиотеку
    windowWidth: 0,
    windowHeight: 0,
    isMobile: false,
    isTablet: false,
    isSmallTablet: false,
    isMobileOrTablet: false,
    isPC: false,
    recaptchaSiteKey: '',

    ageConfirmed: false,
});

export const getters = getterTree(state, {
    GET_RECAPTCHA_SITE_KEY(state): string {
        return state.recaptchaSiteKey;
    },

    ageConfirmed(state): boolean {
        return state.ageConfirmed;
    },
});

export const mutations = mutationTree(state, {
    SET_MOBILE_INFO(state, {
        isMobile,
        isTablet,
        isSmallTablet,
        windowWidth,
        windowHeight,
    }: { isMobile: boolean, isSmallTablet: boolean, isTablet: boolean, windowWidth?: number, windowHeight?: number }) {
        state.isMobile = isMobile;
        state.isSmallTablet = isSmallTablet;
        state.isTablet = isTablet;
        state.isMobileOrTablet = isMobile || isTablet;
        state.isPC = !isMobile && !isTablet;
        if (windowWidth) state.windowWidth = windowWidth;
        if (windowHeight) state.windowHeight = windowHeight;
    },

    SET_RECAPTCHA_SITE_KEY(state, recaptchaSiteKey: string) {
        state.recaptchaSiteKey = recaptchaSiteKey;
    },

    SET_AGE_CONFIRMED(state, status: boolean) {
        state.ageConfirmed = status;
    },
});

export const actions = actionTree({ state, mutations, getters }, {
    async nuxtServerInit({ commit }, { app }) {
        const settings = await this.$request(RGetSettings());
        const recaptchaSiteKey = settings.recaptchaSiteKey;
        app.head.script.push({
            src: `https://www.google.com/recaptcha/api.js?render=${ recaptchaSiteKey }`,
        });

        commit('SET_RECAPTCHA_SITE_KEY', recaptchaSiteKey);
        commit('SET_AGE_CONFIRMED', app.$cookies.get('ageConfirmed') || false);
    },

    setDevice({ commit }) {
        return commit('SET_MOBILE_INFO', {
            isMobile: window.innerWidth < 768,
            isSmallTablet: window.innerWidth < 1024,
            isTablet: window.innerWidth < 1260 && window.innerWidth >= 768,
            windowWidth: window.innerWidth,
            windowHeight: window.innerHeight,
        });
    },
});

// @ts-ignore
export const accessorType = getAccessorType({
    state,
    getters,
    mutations,
    actions,
    modules: {
        modal,
        prompt,
        auth,
        catalog,
        cart,
    },
});
