import { getterTree, mutationTree, actionTree } from 'typed-vuex';
import { IObjectAnyProps } from '~/src/utils/interfaces';

export interface IModal {
  name: string;
  payload?: object;
  showModal?: boolean;
}

export const state = () => ({
    currentModal: '',
    modalsQueue: [] as IModal[],
    showModal: false,
    modalPayload: {} as IObjectAnyProps | undefined,
});

export const getters = getterTree(state, {
    CURRENT: (state: { currentModal: string; }) => state.currentModal,
    SHOW_MODAL: (state: { showModal: boolean; }) => state.showModal,
    MODAL_PAYLOAD: (state: { modalPayload: object | undefined; }) => state.modalPayload,
});

export const mutations = mutationTree(state, {
    SET_CURRENT_MODAL: (state, modal: IModal) => {
        state.modalsQueue.push(modal);

        if (state.modalsQueue.length === 1) {
            state.currentModal = modal.name;
            state.showModal = true;
            state.modalPayload = modal.payload;
        }
    },

    CLOSE_MODAL(state) {
        const currentModalIndex = state.modalsQueue.findIndex((modal: IModal) => modal.name === state.currentModal);
        state.modalsQueue.splice(currentModalIndex, 1);

        state.currentModal = '';
        state.showModal = false;
        state.modalPayload = { errorType: '' };

        if (state.modalsQueue.length) {
            state.currentModal = state.modalsQueue[0].name;
            state.showModal = true;
            state.modalPayload = state.modalsQueue[0].payload;
        }
    },
});

export const actions = actionTree({ state, mutations, getters }, {
    OPEN_MODAL: ({ commit }, item: IModal) => {
        commit('SET_CURRENT_MODAL', item);
    },
    CLOSE_MODAL_ACTION: ({ commit }) => {
        commit('CLOSE_MODAL');
    },
});
