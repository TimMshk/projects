import { getterTree, mutationTree, actionTree } from 'typed-vuex';

export interface IPrompt {
  name: string;
  payload?: object;
  showPrompt?: boolean;
}

export const state = () => ({
    currentPrompt: '',
    showPrompt: false,
    promptPayload: {},
});

export const getters = getterTree(state, {
    CURRENT: (state: { currentPrompt: string; }) => state.currentPrompt,
    SHOW_PROMPT: (state: { showPrompt: boolean; }) => state.showPrompt,
    PROMPT_PAYLOAD: (state: { promptPayload: object | undefined; }) => state.promptPayload,
});

export const mutations = mutationTree(state, {
    SET_CURRENT_PROMPT: (state: { currentPrompt: string; showPrompt: boolean; promptPayload: object | undefined; }, prompt: IPrompt) => {
        state.currentPrompt = prompt.name;
        state.showPrompt = true;
        state.promptPayload = prompt.payload;
    },
    // @ts-ignore
    CLOSE_PROMPT: (state: { currentPrompt: null; showPrompt: boolean; promptPayload: null; }) => {
        state.currentPrompt = null;
        state.showPrompt = false;
        state.promptPayload = null;
    },
});

export const actions = actionTree({ state, mutations, getters }, {
    OPEN_PROMPT: ({ commit }, item: IPrompt) => {
        commit('SET_CURRENT_PROMPT', item);
    },
    CLOSE_PROMPT_ACTION: ({ commit }) => {
        commit('CLOSE_PROMPT');
    },
});
