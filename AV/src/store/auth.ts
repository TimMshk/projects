import { getterTree, mutationTree, actionTree } from 'typed-vuex';
import { ILoginObject, IUser, IUserInfo, IJuridicalPerson } from '~/src/utils/interfaces/auth';
import { RGetUserInfo } from '~/src/utils/api/requests/user';

export const state = () => ({
    accessToken: '',
    user: {} as IUser,
    juridicalPerson: {} as IJuridicalPerson,
    city: '',
    userEmailConfirm: {} as { email: string } | null,
});

export const getters = getterTree(state, {
    accessToken(state): string {
        return state.accessToken;
    },

    user(state): IUser {
        return state.user;
    },

    city(state): string {
        return state.city;
    },

    juridicalPerson(state): IJuridicalPerson {
        return state.juridicalPerson;
    },

    userEmailConfirm(state): { email: string } | null {
        return state.userEmailConfirm;
    },
});

// @ts-ignore
export const mutations = mutationTree(state, {
    setUser(state, loginObject: ILoginObject) {
        state.accessToken = loginObject.authToken;
        state.user = loginObject.user;
        //@ts-ignore
        this.app.context.$cookies.set('accessToken', loginObject.authToken);
        //@ts-ignore
        this.app.context.$cookies.set('refreshToken', loginObject.refreshToken);
    },

    setToken(state, accessToken: string) {
        state.accessToken = accessToken;
    },

    setUserInfo(state, userInfo: IUserInfo) {
        state.user = userInfo.user;
        state.juridicalPerson = userInfo.juridicalPerson;
        state.userEmailConfirm = userInfo.userEmailConfirm;
    },

    changeCity(state, city) {
        state.city = city;
        localStorage.activeCity = city;
    },

    logout(state) {
        state.accessToken = '';
        //@ts-ignore
        this.app.context.$cookies.remove('accessToken');
        //@ts-ignore
        this.app.context.$cookies.remove('refreshToken');
        state.user = {} as IUser;
        state.juridicalPerson = {} as IJuridicalPerson;
        if (window.$nuxt.$route.meta?.middleware?.includes('authenticated')) {
            window.$nuxt.$router.push('/');
        }
        localStorage.removeItem('cart');
        window.$nuxt.$accessor.cart.clearCart();
        window.$nuxt.$accessor.catalog.updateFavorites([]);
    },
});

// @ts-ignore
export const actions = actionTree({ state, mutations, getters }, {
    loginAction: ({ commit, dispatch }, loginObject: ILoginObject) => {
        commit('setUser', loginObject);
    },

    logoutAction: ({ commit }) => {
        // @ts-ignore
        commit('logout');
    },

    async fetchUser({ commit, state }) {
        let token = state.accessToken;
        if (!token) {
            token = this.app.context.$cookies.get('accessToken');
            if (token) {
                commit('setToken', token);
            }
        }

        if (token) {
            const response = await this.$request(RGetUserInfo());
            if (response.user) {
                commit('setUserInfo', response);
            }
        }
    },

    changeCityAction({ commit }, city: string) {
        commit('changeCity', city);
    },
});
