import { accessorType } from '~/src/store';

declare module 'vue/types/vue' {
    interface Vue {
        _uid: number;
        $style: Record<string, string>;
        $accessor: typeof accessorType
    }
}

declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $accessor: typeof accessorType
    }
}
