export interface CookiesSetOptions {
    path: '/' | string,
    expires?: Date,
    /**
     * @description time in seconds
     */
    maxAge?: number
    httpOnly?: boolean
    domain?: string
    encode?: Function
    sameSite: 'None' | 'Strict' | 'Lax' | string
    secure?: boolean
}

type CookiesValue = string | Record<string, any> | any[]

interface Cookies {
    get<T extends CookiesValue>(cookie: string): T | null

    getAll(options?: {
        /**
         * @description Get cookies from res instead of req
         */
        fromRes?: boolean
        parseJSON?: boolean
    }): Array<CookiesValue | null>

    remove(cookie: string, options?: CookiesSetOptions): void

    set(name: string, value: CookiesValue, options: CookiesSetOptions): void

    setAll(cookies: ({
        name: string,
        value: CookiesValue,
        options: CookiesSetOptions
    })[]): void
}

declare module 'vue/types/vue' {
    interface Vue {
        $cookies: Cookies
    }
}

declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $cookies: Cookies
    }
}
