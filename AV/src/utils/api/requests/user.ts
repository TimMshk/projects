import { IRequestConfig } from '~/src/plugins/http-helper';

export const RGetUserInfo = (): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/user/info',
        },
        options: { needToken: true },
    };
};

export const RGetOrders = (): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/user/order-history',
        },
        options: { needToken: true },
    };
};

export const RGetOrderDetails = (params: { id: number }): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: `/user/order-history/${ params.id }`,
        },
        options: { needToken: true },
    };
};

export const RRefreshToken = (props: { refreshTokenObject: object }): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/refresh-token',
            data: props.refreshTokenObject,
        },
        options: { needToken: true },
    };
};

export const RUpdateUser = (props: { userObject: object }): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/user/update',
            data: props.userObject,
        },
        options: { needToken: true },
    };
};

export const RSendConfirmCode = (): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/user/phone-number/send-confirm-code',
        },
        options: { needToken: true },
    };
};

export const RConfirmCode = (props: { obj: object }): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/user/phone-number/confirm-code',
            data: props.obj,
        },
        options: { needToken: true },
    };
};

export const RSendPhone = (props: { obj: object }): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/user/update/phone',
            data: props.obj,
        },
        options: { needToken: true },
    };
};

export const RGetInvoice = (params: { id: number }): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: `/invoice/${ params.id }/pdf`,
            responseType: 'arraybuffer',
        },
        options: { needToken: true },
    };
};

export const RUpdateEmail = (props: { obj: object }): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/user/update/email',
            data: props.obj,
        },
        options: { needToken: true },
    };
};

export const RConfirmEmail = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/user/email/confirm-token',
            data,
        },
        options: { recaptchaAction: 'confirm-phone' },
    };
};
