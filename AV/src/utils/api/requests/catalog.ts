import { IProductRequest } from '~/src/utils/interfaces/catalog';
import { IRequestConfig } from '~/src/plugins/http-helper';

export const RGetCategories = (): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/catalog/category/list',
        },
    };
};

export const RGetFilters = (params: { categoryId: number }): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/catalog/filter',
            data: params,
        },
    };
};

export const RGetProducts = (params: IProductRequest): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/catalog/product/list',
            data: params.requestObj,
        },
    };
};

export const RGetProduct = (params: { id: number }): IRequestConfig => {
    return {
        method: 'get',
        config: { url: `/catalog/product/${ params.id }` },
    };
};

export const RAddFavorite = (requestObj: { productId: number }): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/catalog/favorite/new',
            data: requestObj,
        },
        options: { needToken: true },
    };
};

export const RRemoveFavorite = (requestObj: { productId: number }): IRequestConfig => {
    return {
        method: 'delete',
        config: {
            url: '/catalog/favorite/remove',
            data: requestObj,
        },
        options: { needToken: true },
    };
};

export const RGetFavorites = (): IRequestConfig => {
    return {
        method: 'get',
        config: { url: '/catalog/favorite/list' },
        options: { needToken: true },
    };
};

export const RGetViewed = (): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/recently-viewed',
        },
        options: { needToken: true },
    };
};

export const RAddViewed = (requestObj: { productId: number }): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/recently-viewed',
            data: requestObj,
        },
        options: { needToken: true },
    };
};
