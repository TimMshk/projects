import { IRequestConfig } from '~/src/plugins/http-helper';

export interface IFeedbackObj {
    city: string,
    fullName: string,
    communicationType: string,
    theme: string,
    message: string,
    cardNumber: string | number,
    phoneNumber?: string | number,
    email?: string,
}

export const RSendFeedback = (obj: IFeedbackObj): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/feedback',
            data: obj,
        },
        options: { recaptchaAction: 'feedback' },
    };
};
