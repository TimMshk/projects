import { IRequestConfig } from '~/src/plugins/http-helper';

export const RGetSettings = (): IRequestConfig => {
    return {
        method: 'get',
        config: { url: '/settings' },
    };
};

export const RLogin = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/login',
            data,
        },
        options: { recaptchaAction: 'auth' },
    };
};

export const RConfirmLoginPhone = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/login/confirm',
            data,
        },
        options: { recaptchaAction: 'auth-confirm-phone' },
    };
};

export const RGetCompanies = (data: object): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/auth/registration/search-companies',
            data,
        },
        options: { recaptchaAction: 'search-companies' },
    };
};

export const RGetAddresses = (data: object): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/auth/registration/search-addresses',
            data,
        },
        options: { recaptchaAction: 'search-addresses' },
    };
};

export const RGetCompany = (data: object): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/auth/registration/find-company',
            data,
        },
        options: { recaptchaAction: 'search-company' },
    };
};

export const RFindBank = (data: object): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/auth/registration/find-bank',
            data,
        },
        options: { recaptchaAction: 'find-bank' },
    };
};

export const RValidateRegistration = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/registration/validate',
            data,
        },
        options: { recaptchaAction: 'validate-registration' },
    };
};

export const RSendRegistrationConfirmPhone = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/registration/send-confirm-code',
            data,
        },
        options: { recaptchaAction: 'confirm-phone' },
    };
};

export const RRegistrationConfirmPhone = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/registration/confirm',
            data,
        },
        options: { recaptchaAction: 'confirm-phone' },
    };
};

export const RRestorePassword = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/restore-password',
            data,
        },
        options: { recaptchaAction: 'confirm-email' },
    };
};

export const RNewPassword = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/new-password',
            data,
        },
        options: { recaptchaAction: 'change-password' },
    };
};

export const RRestorePasswordConfirmPhone = (data: object): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/auth/new-password/confirm',
            data,
        },
        options: { recaptchaAction: 'confirm-phone' },
    };
};
