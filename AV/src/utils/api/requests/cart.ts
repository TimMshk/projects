import { IRequestConfig } from '~/src/plugins/http-helper';
import { ICartItem, IOrderRequestInfo } from '~/src/utils/interfaces/cart';

interface IUpdateQuantityRequest {
    product: ICartItem,
}

export const RGetCart = (): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/cart',
        },
        options: {
            needToken: true,
        },
    };
};

export const RUpdateQuantity = (requestObj: IUpdateQuantityRequest): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/cart/update-quantity',
            data: {
                productId: requestObj.product.product.id,
                quantity: requestObj.product.quantity,
            },
        },
        options: {
            needToken: true,
        },
    };
};

export const ROverwriteCart = (props: {cart: ICartItem[] }): IRequestConfig => {
    const items = props.cart.map((item) => {
        return {
            productId: item.product.id,
            quantity: item.quantity,
        };
    });
    return {
        method: 'post',
        config: {
            url: '/cart/overwrite',
            data: { items },
        },
        options: {
            needToken: true,
        },
    };
};

export const RCreateAccount = (props: IOrderRequestInfo): IRequestConfig => {
    return {
        method: 'post',
        config: {
            url: '/order/create',
            data: props.orderInfo,
        },
        options: {
            needToken: true,
        },
    };
};

export const RGetDeliveryPrice = (): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/settings/delivery-price',
        },
    };
};
