import { IRequestConfig } from '~/src/plugins/http-helper';

export const RGetCompilation = (name: string): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: `/catalog/compilation/${ name }`,
        },
    };
};
