import { IRequestConfig } from '~/src/plugins/http-helper';
import { IProductRequest } from '~/src/utils/interfaces/catalog';

export const RAutocompleteSearch = (searchQuery: string): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/catalog/product/autocomplete-search',
            data: { searchQuery },
        },
    };
};

export const RSearchProducts = (params: IProductRequest): IRequestConfig => {
    return {
        method: 'get',
        config: {
            url: '/catalog/product/search',
            data: params.requestObj,
        },
    };
};
