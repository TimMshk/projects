const goBack = (defaultPath?: string) => ({
    methods: {
        back() {
            // @ts-ignore
            $nuxt?.context.from ? this.$router.go(-1) : this.$router.push(defaultPath || '/');
        },
    },
});

export default goBack;
