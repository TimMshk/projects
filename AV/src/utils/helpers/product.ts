import { IProduct } from '~/src/utils/interfaces/catalog';

export const minWeightCount = (product: IProduct): number => {
    if (product.productType !== 'weight') return 1;

    if (product.standardWeight && product.weightStep) {
        if (product.quantity && product.quantity * 1000 < product.standardWeight) {
            return product.weightStep / 1000;
        }

        return ((product.weightStep / 1000 > product.standardWeight) ? product.weightStep : product.standardWeight) / 1000;
    }

    return product.standardWeight || product.weightStep! / 1000 || 0.1;
};
