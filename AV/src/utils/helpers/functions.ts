export const validPhone = (num: string): string => {
    return String(num).replace(/^([\d*])([\d*]{3})([\d*]{3})([\d*]{2})([\d*]{2})$/, '+$1 ($2) $3-$4-$5');
};

export const numberToString = (num: string): string => {
    return num.replace(/[^\d]/g, '');
};

export const priceFormat = (price: number): string => {
    return new Intl.NumberFormat('ru-RU', { maximumFractionDigits: 2, minimumFractionDigits: 2 }).format(price);
};

export const consistFormat = (volume: number): string => {
    return new Intl.NumberFormat('en-EN', { maximumFractionDigits: 2 }).format(volume);
};

export const weightFormat = (weight: number): string => {
    return new Intl.NumberFormat('ru-RU', { maximumFractionDigits: 3 }).format(weight);
};

export const triggerDownloadClick = (url: string, downloadTitle: string): void => {
    const fileLink = document.createElement('a');
    fileLink.href = url;
    fileLink.setAttribute('download', downloadTitle);
    document.body.appendChild(fileLink);
    fileLink.click();
    window.URL.revokeObjectURL(fileLink.href);
    document.body.removeChild(fileLink);
};
