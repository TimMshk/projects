import { IBreadcrumb } from '~/src/utils/interfaces/catalog';

export const managerPhone = '74952230244';

export const navigationList = [
    {
        name: 'Алкоголь',
        link: 'https://av.ru/lp/wineclub/',
    },
    {
        name: 'Торты',
        link: 'https://av.ru/tort/',
    },
    {
        name: 'Кейтеринг',
        link: 'https://catering.av.ru/',
    },
    {
        name: 'Микромаркеты',
        link: 'https://av.ru/lp/micromarket/',
    },
    {
        name: 'Азбука вкуса для физ.лиц',
        link: 'https://av.ru/',
    },
];

export const nuxtLinkList = [
    {
        name: 'Доставка и оплата',
        link: '/delivery',
    },
    {
        name: 'О компании',
        link: '/about',
    },
];

export const breadcrumbsDefaultPath = [
    {
        name: 'Главная',
        to: {
            name: 'index',
            params: {},
        },
    },
    {
        name: 'Каталог',
        to: {
            name: 'catalog',
            params: {},
        },
    },
] as IBreadcrumb[];

export const POLICY_LINK = '/shared/user_privacy_policy_gs.pdf';
export const STANDARD_CONTRACT_LINK = '/shared/av_contract.doc';
