import { ICartItem } from '~/src/utils/interfaces/cart';

export const mergeCart = (baseCart: ICartItem[], extendCart: ICartItem[]) => {
    return baseCart.reduce((result, item) => {
        const sample = result.find((resultItem: ICartItem) => resultItem.product.id === item.product.id);
        if (sample) {
            sample.quantity = Math.max(item.quantity, sample.quantity);
        }
        else {
            result.push(item);
        }
        return result;
    }, extendCart);
};
