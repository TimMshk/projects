import { IObjectAnyProps } from '~/src/utils/interfaces';

interface IResponseError {
    field: string,
    message: string,
}

export const createResponseErrorsObject = (responseErrors: IResponseError[]): IObjectAnyProps => {
    const errors: IObjectAnyProps = {};
    responseErrors.forEach((field: IObjectAnyProps) => {
        !errors[field.field] ? errors[field.field] = `<li>${ field.message }</li>` : errors[field.field] += `<li>${ field.message }</li>`;
    });
    return errors;
};
