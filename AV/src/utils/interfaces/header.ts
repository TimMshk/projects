export interface ICard {
    id: number,
    link: string,
    status: string,
    checked: boolean,
    images: object[],
    info: object,
}
