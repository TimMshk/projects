export interface IObjectAnyProps {
    [key: string]: any
}

export type TStringOrNumber = string | number;
export type TStringOrBoolean = string | boolean;
export type TNumberOrNull = number | null;
export type TObjectOrNull = object | null;
export type TObjectAnyPropsOrNull = IObjectAnyProps | null;
