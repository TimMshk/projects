export interface ICartItem {
    product: {
        articul: string,
        images: { path: string }[],
        title: string,

        id: number,
        price: number,
        unitPrice: number,
        weightPerItem: number,
        count: number,
        quantity: number,
        featureNonStandard?: string,
    },
    price?: number,
    quantity: number,
    save?: boolean,
}

export interface IAddressItem {
    displayName: string,
    html: string,
    value: string,
}

export interface IDeliveryInfo {
    address: string,
    selectedDate: string,
    selectedTime: string,
}

export interface IOrderRequestInfo {
    orderInfo: {
        deliveryDate: string,
        deliveryTimeStart: string,
        deliveryTimeEnd: string,
        deliveryAddress: string,
    },
}

export interface IOrderInfo {
    id: number,
    erpId: string,
    total: number,
    createTime: string,
    companyName: string,
    recipientName: string,
    recipientPhone: string,
    recipientEmail: string,
}

export interface IOrderingError {
    code: string,
    message: string,
}

export interface IOrdering {
    isAvailable: boolean,
    unavailabilityReasons: IOrderingError[],
}
