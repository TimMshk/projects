import { IProduct } from '~/src/utils/interfaces/catalog';

export interface ICompilation {
    products: IProduct[],
    title: string,
}
