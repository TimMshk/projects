import { IObjectAnyProps } from '~/src/utils/interfaces/index';

export interface IProductRequest {
    requestObj: {
        searchQuery?: string,
        categoryId: number,
        filter?: IObjectAnyProps,
        pagination: {
            limit?: number,
            page: number,
        },
        orderBy?: string,
    },
}

export interface ICategory {
    id: number,
    sort: number,
    name: string,
    description: string,
    treeParentId?: string,
    children?: ICategory[],
    url: string,
}

export interface ICategories {
    treeRootId: null | number,
    categories: ICategory[],
}

export interface IProductsItem {
    brandId: number,
    id: number,
    favorite: boolean,
    images: object[],
    title: string,
    price: number,
    articul?: string,
    countInCart?: number,
    quantity?: number,
    weightPerItem?: number,
    featureNonStandard?: string,
}

export interface IProductsPagination {
    countPages: number,
    countTotal: number,
}

export interface IProductsResponse {
    items: IProductsItem[],
    pagination: IProductsPagination,
}

export interface IBreadcrumb {
    name: string,
    to: {
        name: string,
        params: {
            id?: string,
            category?: string,
            url?: string,
        },
    },
}

export interface IProductCategory {
    id: number,
    name: string,
    url: string,
}

export interface IProduct {
    favorite: boolean,

    id: number,
    standardWeight: number,
    price: number,
    unitPrice: number,
    quantity: number,
    countInCart: number,
    protein?: number,
    fats?: number,
    carbohydrate?: number,
    energyValue?: number,
    energyValueJ?: number,
    weightPerItem?: number,
    weightStep?: number,

    articul: string,
    title: string,
    showUnit: string,
    description: string,
    composition: string,
    productType: string,
    featureNonStandard: string,

    images: { path: string }[],
    categories?: IProductCategory[],

    description1?: string,
    description2?: string,
    description3?: string,
    countryManufactured?: string,
    companyManufacturer?: string,
    storageConditionLifeUnit?: string,
    storageConditionLife?: string,
    storageConditionLifeExtended?: string,
    storageConditionTemperature?: string,
    storageConditionText?: string,
    regulatoryDocument?: string,
    benefit?: string,
    wineGrape?: string,
    wineType?: string,
    country?: string,
    region?: string,
    usedInfo?: string,
    lifeToCustomer?: number,
    wineRegionId?: number,
    wineSugar?: number | string,
    wineAlcohol?: number | string,
    wineYear?: number | string,
}

export interface IListParam {
    title: string,
    value: number,
    checked: boolean,
}

export interface IFilter {
    title: string,
    name: string,
    type: string,
    params: IObjectAnyProps,
    filteredParams: any,
    open?: boolean,
    searchString?: string,
}
