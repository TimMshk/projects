import { TStringOrBoolean } from '~/src/utils/interfaces/index';

export interface IToken {
    tokenType: string,
    accessToken: string,
}

export interface IRegistrationCompany {
    name: string,
    inn: string,
    html: string,
}

export interface IAddressItem {
    address: string,
    fiasId: string,
    html: string,
}

export interface ILogin {
    email: TStringOrBoolean,
    password: TStringOrBoolean,
}

export interface ILoginErrors {
    email: TStringOrBoolean,
    password: TStringOrBoolean,
}

export interface IRegistrationCompanyInfo {
    inn: string,
    kpp: string,
    bik: string,
    checkingAccount: string,
}

export interface ISelectedCompany {
    address: string,
    inn: string,
    kpp: string,
    name: string,
}

export interface IUser {
    id: number,
    fullName: string,
    roles: string[],
    email: string,
    discount: string,
    phoneNumber: string,
    isEmailConfirmed: boolean,
}

export interface IJuridicalPerson {
    id: number,
    juridicalName: string,
    juridicalAddress: string,
    inn: string,
    ogrn: string,
    kpo: string,
    bankName: string,
    bik: string,
    checkingAccount: string,
    correspondingAccount: string,
    actualAddress: string,
}

export interface IUserInfo {
    user: IUser,
    juridicalPerson: IJuridicalPerson,
    userEmailConfirm: { email: string } | null,
}

export interface ILoginObject {
    authToken: string,
    user: IUser,
}

export interface IUserFavorite {
    id: number,
    price: number,
    weightPerItem: number,
    title: string,
    articul: string,
    images: { path: string }[],
}

export interface IOrderDetailsProduct {
    id: number,
    articul: string,
    title: string,
    price: number,
    priceTotal: number,
    quantity: number,
    weight: number,
}

export interface IOrderDetails {
    id: number,
    articul: string,
    deliveryDate: string,
    createdAt: string,
    deliveryAddress: string,
    price: number,
    status: {
        status: string,
        name: string,
        isCompletedOrder: boolean,
    },
    comment: string,
    products: IOrderDetailsProduct[],
}

export interface IInvoice {
    title: string,
    url: string,
}
