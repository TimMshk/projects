import Vue from 'vue';
// @ts-ignore
import vClickOutside from 'vue-click-outside';

Vue.directive('click-outside', vClickOutside);
