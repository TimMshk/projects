import Vue from 'vue';
// @ts-ignore
import VScrollLock from 'v-scroll-lock';

Vue.use(VScrollLock, {
    bodyScrollOptions: {
        reserveScrollBarGap: true,
    },
});
