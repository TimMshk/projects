import { NuxtConfig, Plugin } from '@nuxt/types';
import { AxiosError, AxiosRequestConfig } from 'axios';
import qs from 'qs';
import { RRefreshToken } from '~/src/utils/api/requests/user';

interface axiosConfigWithRetry extends AxiosRequestConfig {
    _retry?: boolean,
}

interface requestOptions {
    recaptchaAction?: string,
    needToken?: boolean,
}

export interface IRequestConfig {
    method: string,
    config: AxiosRequestConfig,
    options?: requestOptions,
}

const request = (context: NuxtConfig) => {
    context.$axios.onError(
        (error: AxiosError) => {
            // network error
            if (!error.response) {
                context.$accessor.prompt.OPEN_PROMPT({ name: 'ReloadPage' });
            }
            else {
                const originalRequest: axiosConfigWithRetry = error.config;

                if (error.response!.status === 401 && !originalRequest._retry) {
                    originalRequest._retry = true;
                    return refreshOrLogout(originalRequest);
                }

                error.response!.data.RESPONSE_STATUS = error.response!.status;
                return Promise.reject(error.response!.data);
            }
        },
    );

    const refreshOrLogout = (originalRequest: axiosConfigWithRetry) => {
        const refreshToken = context.$cookies.get('refreshToken');
        if (!refreshToken) return;
        return context.$request(RRefreshToken({
            refreshTokenObject: {
                refreshToken,
            },
        }))
            .then((response: any) => {
                if (response.RESPONSE_STATUS === 200) {
                    context.$accessor.auth.loginAction(response);
                    originalRequest.headers['X-Auth-Token'] = response.authToken;
                    return context.$axios.request(originalRequest);
                }
                else if (response.RESPONSE_STATUS === 400 || response.RESPONSE_STATUS === 401 || response.RESPONSE_STATUS === 422) {
                    context.$accessor.auth.logoutAction();
                    context.$accessor.modal.OPEN_MODAL({ name: 'AboutError', payload: { errorType: 'NeedAuth' } });
                    return Promise.reject(response);
                }
            });
    };

    const getRecaptcha = (action: string) => {
        const recaptchaSiteKey = context.$accessor.GET_RECAPTCHA_SITE_KEY;
        return new Promise((resolve) => {
            // @ts-ignore
            grecaptcha.ready(() => {
                // @ts-ignore
                grecaptcha.execute(recaptchaSiteKey, { action }).then((token: string) => {
                    resolve(token);
                });
            });
        });
    };

    return async (config: IRequestConfig) => {
        const url = config.config.url || '';
        const responseType = config.config.responseType || '';
        const method = config.method || '';
        const headers: AxiosRequestConfig['headers'] = config.config.headers || {};
        let data = config.config.data || {};

        try {
            let requestObj: object = {
                url, method, responseType,
            };

            if (config.options?.needToken) {
                headers['X-Auth-Token'] = context.$cookies.get('accessToken');
            }

            if (config.options?.recaptchaAction) {
                data = { ...data, recaptchaToken: await getRecaptcha('recaptchaAction') };
            }

            if (config.method === 'get') {
                requestObj = {
                    ...requestObj,
                    params: data,
                    headers,
                    paramsSerializer: (params: object) => {
                        return qs.stringify(params);
                    },
                };
            }
            else {
                requestObj = { ...requestObj, data, headers };
            }

            const response = await context.$axios.request(requestObj);

            response.data.RESPONSE_STATUS = response.status;

            return response.data;
        }
        catch (e) {
            console.log(e);
            return e;
        }
    };
};

declare module 'vue/types/vue' {
    interface Vue {
        $request(requestConfig: IRequestConfig): any
    }
}

declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $request(requestConfig: IRequestConfig): any
    }
    interface Context {
        $request(requestConfig: IRequestConfig): any
    }
}

declare module 'vuex/types/index' {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    interface Store<S> {
        $request(requestConfig: IRequestConfig): any
    }
}

const myPlugin: Plugin = (context, inject) => {
    //@ts-ignore
    inject('request', request(context));
};

export default myPlugin;
