import { Plugin } from '@nuxt/types';

//Вам нужно продублировать типы 4 раза :(
//Так советуется даже в офиициальной документации
declare module '@nuxt/types' {
    interface NuxtAppOptions {
        $testMethod (param: string): number
    }

    interface Context {
        $testMethod (param: string): number
    }
}

declare module 'vue/types/vue' {
    interface Vue {
        $testMethod (param: string): number
    }
}

declare module 'vuex/types/index' {
    interface Store<S> {
        $testMethod (this: S, param: string): number
    }
}

const ExampleMixin: Plugin = (context, inject) => {
    function testMethod (param: string) {
        return param;
    }

    //Эта функция будет встроена в Vue как this.$testMethod
    //Эта функция будет встроена в Nuxt как context.$testMethod
    //Эта функция будет встроена в Vuex как this.$testMethod
    inject('testMethod', testMethod);
};

export default ExampleMixin;
