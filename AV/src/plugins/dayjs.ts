import { Context, Plugin } from '@nuxt/types';

const dayjsPlugin: Plugin = (ctx: Context) => {
    ctx.app.$dayjs.updateLocale('ru', {
        months: 'января_февраля_марта_апреля_мая_июня_июля_августа_сентября_октября_ноября_декабря'.split('_'),
    });
};

export default dayjsPlugin;
