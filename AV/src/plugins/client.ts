import { Context, Plugin } from '@nuxt/types';

const clientSidePlugin: Plugin = (ctx: Context) => {
    ctx.app.$accessor.setDevice();
    window.addEventListener('resize', () => ctx.app.$accessor.setDevice());
};

export default clientSidePlugin;
