import { Plugin } from '@nuxt/types';
import ExampleMixin from '~/src/plugins/mixins/example';

const serverSidePlugin: Plugin = async (context, inject) => {
    const mixins: Array<Plugin> = [ExampleMixin];

    await Promise.all(mixins.map(mixin => mixin(context, inject)));
};

export default serverSidePlugin;
