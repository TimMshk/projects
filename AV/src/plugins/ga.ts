export default () => {
    const gtagParam = process.env.gtagParam;

    if (!gtagParam) return;

    //@ts-ignore
    window.dataLayer = window.dataLayer || [];

    function gtag(...args: any[]) {
        //@ts-ignore
        dataLayer.push(args);
    }

    gtag('js', new Date());

    gtag('config', gtagParam);
};
