import Vue from 'vue';
// @ts-ignore
import { CalendarView, CalendarViewHeader, CalendarViewWeek } from 'vue-simple-calendar';

Vue.component('CalendarView', CalendarView);
Vue.component('CalendarViewHeader', CalendarViewHeader);
Vue.component('CalendarViewWeek', CalendarViewWeek);
